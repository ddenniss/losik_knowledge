 #lang racket

(require "../../../odysseus/lib/_all.rkt")
(require "../../../odysseus/tabtree/tab-tree.rkt")
(require "../../../odysseus/tabtree/utils.rkt")

(define file-to-sort "../nasevere/murmansk.tree")

(define-namespace-anchor anchor)
(define ns-own (namespace-anchor->namespace anchor))


(tabtree-sort-and-print file-to-sort #:ns ns-own)
